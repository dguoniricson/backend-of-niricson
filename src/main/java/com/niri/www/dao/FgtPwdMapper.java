package com.niri.www.dao;

import com.niri.www.entity.FgtPwd;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FgtPwdMapper {
    public FgtPwd getFgtPwd(String email);
    public FgtPwd getBy(FgtPwd fp);
    public void insert(FgtPwd fp);
    public int update(FgtPwd fp);
}
