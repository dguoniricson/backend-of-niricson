package com.niri.www.dao;

import com.alibaba.fastjson.JSONObject;
import com.niri.www.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    public User getUser(@Param("name") String name, @Param("pw") String pw);

    public List<User> getBy(User user);

    public List<User> getByOr(User user);

    public int updateByEmail(User user);

    public User getUserConfig(String name);

    public int updateByUser(User user);

    public List<JSONObject> getUserList(String cty, String prv);

    public int addUser(User user);

}
