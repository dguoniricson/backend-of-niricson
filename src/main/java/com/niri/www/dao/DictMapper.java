package com.niri.www.dao;

import com.niri.www.entity.Country;
import com.niri.www.entity.Province;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DictMapper {
    public String getKV(String key);
    public List<Country> getCountry();
    public List<Province> getProvince(String ctyCode);

}
