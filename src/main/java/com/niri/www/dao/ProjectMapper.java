package com.niri.www.dao;

import com.alibaba.fastjson.JSONObject;
import com.niri.www.entity.Project;
import com.niri.www.entity.ProjectHtml;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProjectMapper {

    public List<Project> getUserProjs(String user);
    public String getPrjWebcontent(String id);
    public List<Project> getBy(Project prj);
    public int updateByMap(JSONObject prj);
    public int updateByPrj(Project prj);
    public int addPrj(Project prj);
    public List<ProjectHtml> getPrjHtml(String id);
    public int updatePrjHtml(ProjectHtml projectHtml);
    public int addPrjHtml(ProjectHtml projectHtml);
    public String getPrjAssets(String id);
    public String getDftColor(String id);
    public String getECharts(String id);
}
