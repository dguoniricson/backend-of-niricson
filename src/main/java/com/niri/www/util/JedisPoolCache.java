package com.niri.www.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Component
public class JedisPoolCache {
    @Value("${spring.redis.host}")
    private String redisUrl;

    private JedisPool jedisPool;

    public Jedis getCache(){
        if(jedisPool == null)
            jedisPool = new JedisPool(redisUrl);
        return jedisPool.getResource();
    }
}
