package com.niri.www.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class ResourceReader {

    public static String getContent(String path){
        String content = null;
        BufferedReader reader = null;
        try {
            ClassPathResource cpr = new ClassPathResource(path);
            reader = new BufferedReader(new InputStreamReader(cpr.getInputStream()));
            content = reader.lines().collect(Collectors.joining("\n"));
            reader.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return content;
    }
}
