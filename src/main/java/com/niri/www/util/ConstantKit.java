package com.niri.www.util;

public final class ConstantKit {
    public static final Integer TOKEN_EXPIRE_TIME = 3600 * 24;
    public static final Integer TOKEN_RESET_TIME = 1000 * TOKEN_EXPIRE_TIME;
    public static final String REQUEST_TOKEN_VALUE = "REQUEST_TOKEN_VALUE";
}
