package com.niri.www;

import com.alibaba.fastjson.JSONObject;
import com.niri.www.entity.User;
import com.niri.www.service.ProjDefectService;
import com.niri.www.service.UserService;
import com.niri.www.util.SpringUtil;
import com.niri.www.util.JedisPoolCache;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import redis.clients.jedis.Jedis;

import java.util.List;

import static java.lang.System.exit;


@SpringBootApplication
public class NiriBackApplication {

    private static Logger logger = LoggerFactory.getLogger(NiriBackApplication.class);

    public static void testRedis(){

        try{
            Jedis jedis = SpringUtil.getBean(JedisPoolCache.class).getCache();
        }catch (Exception ex){
            logger.info("Redis is not ready, server launch failed ...");
            exit(1);
        }
    }

    public static void testMongo(){

      ProjDefectService projDefectService = SpringUtil.getBean(ProjDefectService.class);
      JSONObject obj = projDefectService.getProjDft("CA-BC-DM-1");
      logger.info(JSONObject.toJSONString(obj));

    }

    public static void main(String[] args) {
          SpringApplication.run(NiriBackApplication.class, args);

          long time = 0;
          time = 0;
          long time1 = System.currentTimeMillis();

          testRedis();

          //testMongo();

          long time2 = System.currentTimeMillis();
          time += time2 - time1;
          logger.info("back millisec：" + time);

      }

}
