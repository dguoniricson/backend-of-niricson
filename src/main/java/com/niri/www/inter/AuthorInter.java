package com.niri.www.inter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.niri.www.util.AllowAnon;
import com.niri.www.model.TokenValue;
import com.niri.www.util.ConstantKit;
import com.niri.www.util.JedisPoolCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.lang.reflect.Method;

public class AuthorInter implements HandlerInterceptor {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private String httpHeaderName = "Authorization";

    private String unauthorizedErrorMessage = "401 unauthorized";

    private int unauthorizedErrorCode = HttpServletResponse.SC_UNAUTHORIZED;

    @Autowired
    private JedisPoolCache jedisPoolCache;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        if (method.getAnnotation(AllowAnon.class) != null || handlerMethod.getBeanType().getAnnotation(AllowAnon.class) != null) {
        } else {

            String token = request.getHeader(httpHeaderName);

            if(StringUtils.isEmpty(token)){
                JSONObject jsonObject = new JSONObject();

                PrintWriter out = null;
                try {
                    response.setStatus(unauthorizedErrorCode);
                    response.setContentType(MediaType.APPLICATION_JSON_VALUE);

                    jsonObject.put("code", ((HttpServletResponse) response).getStatus());
                    jsonObject.put("message", HttpStatus.UNAUTHORIZED);
                    out = response.getWriter();
                    out.println(jsonObject);

                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                } finally {
                    if (null != out) {
                        out.flush();
                        out.close();
                    }
                }// try {}
            }

            String username = "";

            Jedis jedis = jedisPoolCache.getCache();
            TokenValue tokenValue = null;
            tokenValue = JSON.parseObject(jedis.get(token), TokenValue.class);
            jedis.close();

            if(tokenValue != null) {
                username = tokenValue.getUsername();
            }

            if (tokenValue!= null && tokenValue.getCreateTime() != 0) {
                Long tokeBirthTime = tokenValue.getCreateTime();
                Long diff = System.currentTimeMillis() - tokeBirthTime;
                if(diff <= ConstantKit.TOKEN_RESET_TIME){
                    request.setAttribute(ConstantKit.REQUEST_TOKEN_VALUE, tokenValue);
                    return true;
                }
            }

            JSONObject jsonObject = new JSONObject();

            PrintWriter out = null;
            try {
                response.setStatus(unauthorizedErrorCode);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);

                jsonObject.put("code", ((HttpServletResponse) response).getStatus());
                jsonObject.put("message", HttpStatus.UNAUTHORIZED);
                out = response.getWriter();
                out.println(jsonObject);

                return false;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (null != out) {
                    out.flush();
                    out.close();
                }
            }// try {}
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
