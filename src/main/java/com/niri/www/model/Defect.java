package com.niri.www.model;

public class Defect {
    String id;
    String name;
    String aw;
    String leng;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAw() {
    return aw;
  }

  public void setAw(String aw) {
    this.aw = aw;
  }

  public String getLeng() {
    return leng;
  }

  public void setLeng(String leng) {
    this.leng = leng;
  }
}
