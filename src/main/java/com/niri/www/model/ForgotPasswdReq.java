package com.niri.www.model;

public class ForgotPasswdReq {
    String ip;
    String email;

    public ForgotPasswdReq(String ip, String em){
        this.ip = ip;
        this.email = em;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
