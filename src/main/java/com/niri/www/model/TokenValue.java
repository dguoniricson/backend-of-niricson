package com.niri.www.model;

public class TokenValue {
    //public String token;
    public static final int TYPE_INTERNAL = 1;
    public static final int TYPE_CUSTOMER = 2;

    String  username;
    int     type;
    long    createTime;

    public TokenValue(){
    }

    public TokenValue(String un, int tp, long ct){
        username = un;
        type = tp;
        createTime = ct;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
