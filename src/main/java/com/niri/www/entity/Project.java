package com.niri.www.entity;

public class Project {
    String id;
    String name;
    String user;
    int type;
    double lon;
    double lat;
    String mapPath;
    String bbox;
    String epsg;
    String state;
    String country;
    String picUrl;
    String effPath;
    String vegpath;
    String splpath;
    String dftPath;
    String thmpath;


    public String getThmpath() {
        return thmpath;
    }

    public void setThmpath(String thmpath) {
        this.thmpath = thmpath;
    }

    public String getSplpath() {
        return splpath;
    }

    public void setSplpath(String splpath) {
        this.splpath = splpath;
    }

    public String getVegpath() {
        return vegpath;
    }

    public void setVegpath(String vegpath) {
        this.vegpath = vegpath;
    }

    String prjsts;
    int status;
    int newQuest;
    int newAnswer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getMapPath() {
        return mapPath;
    }

    public void setMapPath(String mapPath) {
        this.mapPath = mapPath;
    }

    public String getBbox() {
        return bbox;
    }

    public void setBbox(String bbox) {
        this.bbox = bbox;
    }

    public String getEpsg() {
        return epsg;
    }

    public void setEpsg(String epsg) {
        this.epsg = epsg;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getEffPath() {
        return effPath;
    }

    public void setEffPath(String effPath) {
        this.effPath = effPath;
    }

    public String getDftPath() {
        return dftPath;
    }

    public void setDftPath(String dftPath) {
        this.dftPath = dftPath;
    }

    public String getPrjsts() {
        return prjsts;
    }

    public void setPrjsts(String prjsts) {
        this.prjsts = prjsts;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNewQuest() {
        return newQuest;
    }

    public void setNewQuest(int newQuest) {
        this.newQuest = newQuest;
    }

    public int getNewAnswer() {
        return newAnswer;
    }

    public void setNewAnswer(int newAnswer) {
        this.newAnswer = newAnswer;
    }
}
