package com.niri.www.controller;

import com.niri.www.entity.User;
import com.niri.www.model.ResponseTemplate;
import com.niri.www.util.ConstantKit;
import com.niri.www.model.TokenValue;
import com.niri.www.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    public static final int USER_TYPE_ADMIN = 1;
    public static final int USER_TYPE_CLIENT = 2;
    public static final int USER_TYPE_DRONE_OP = 4;
    public static final int USER_STATUS_ACTIVE = 1;
    public static final int USER_STATUS_REMOVE = 0;

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/cfg", method = RequestMethod.GET)
    public ResponseTemplate getUserConfig(String name, HttpServletRequest request) {

        logger.info("UserController.getUserConfig()");
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());

        TokenValue tokenValue = (TokenValue)request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);

        if(StringUtils.isEmpty(name) || tokenValue.getUsername().equals(name) == false)
            return ret;

        ret.setData(userService.getUserConfig(name));
        ret.setMessage("get cfg succ");

        return ret;
    }

    @RequestMapping(value = "/ucfg", method = RequestMethod.POST)
    public ResponseTemplate saveUserConfig(@RequestBody User user, HttpServletRequest request) {

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        TokenValue tokenValue = (TokenValue)request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);

        if(user == null || StringUtils.isEmpty(user.getUsername())
                || !user.getUsername().equals(tokenValue.getUsername())
                || (user.getType()!=null && (user.getType() & USER_TYPE_ADMIN) > 0)
        )
            return ret;
        if(!StringUtils.isEmpty(user.getPassword())){
            String pass = DigestUtils.sha256Hex(user.getPassword());
            user.setPassword(pass);
        }
        ret.setData(userService.updateByUser(user));
        ret.setMessage("set cfg succ");
        return ret;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseTemplate getUserList(@RequestBody User user, HttpServletRequest request) {
        logger.info("UserController.getUserList()");
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());

        TokenValue tokenValue = (TokenValue)request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);

        if(user == null || StringUtils.isEmpty(user.getCountry()) || (tokenValue.getType() & USER_TYPE_ADMIN)==0)
            return ret;
        if(user.getType() == null || (user.getType()&USER_TYPE_ADMIN)>0)
            user.setType(USER_TYPE_CLIENT | USER_TYPE_DRONE_OP);

        if("all".equals(user.getCountry()))
            user.setCountry(null);
        if("all".equals(user.getProvince()))
            user.setProvince(null);


        user.setStatus(USER_STATUS_ACTIVE);
        List<User> userList = userService.getBy(user);

        ret.setData(userList);
        ret.setMessage("get user list succ");
        return ret;
    }

    @RequestMapping(value = "/up", method = RequestMethod.POST)
    public ResponseTemplate updateUser(@RequestBody User user, HttpServletRequest request) {
        logger.info("UserController.updateUser() type = "+user.getType());

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        TokenValue tokenValue = (TokenValue)request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);

        if(user == null || (tokenValue.getType() & USER_TYPE_ADMIN) == 0)
            return ret;
        if(user.getType() != null && (user.getType() & USER_TYPE_ADMIN)>0 )
            user.setType(USER_TYPE_CLIENT);
        user.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        if(!StringUtils.isEmpty(user.getPassword())){
            String pass = DigestUtils.sha256Hex(user.getPassword());
            user.setPassword(pass);
        }
        ret.setData(userService.updateByUser(user));
        ret.setMessage("update user succ");
        return ret;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseTemplate addUser(@RequestBody User user, HttpServletRequest request) {
        logger.info("UserController.addUser()");

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(0);

        TokenValue tokenValue = (TokenValue) request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);

        if(user == null || StringUtils.isEmpty(user.getUsername()) || (tokenValue.getType() & USER_TYPE_ADMIN) == 0)
            return ret;
        if((user.getType()&USER_TYPE_ADMIN)>0)
            user.setType(USER_TYPE_CLIENT);

        user.setStatus(USER_STATUS_ACTIVE);
        user.setCreateDate(new Timestamp(System.currentTimeMillis()));
        if(!StringUtils.isEmpty(user.getPassword())){
            String pass = DigestUtils.sha256Hex(user.getPassword());
            user.setPassword(pass);
        }

        User target = new User();
        target.setUsername(user.getUsername());
        target.setEmail(user.getEmail());
        List<User> userList = userService.getByOr(target);
        if(userList.size()>0)
            ret.setData(0);
        else
            ret.setData(userService.addUser(user));
        ret.setMessage("add user succ");
        return ret;
    }
}
