package com.niri.www.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.niri.www.baidu.ActionEnter;
import com.niri.www.model.Defect;
import com.niri.www.model.ResponseTemplate;
import com.niri.www.service.ProjDefectService;
import com.niri.www.service.S3Service;
import com.niri.www.util.ConstantKit;
import com.niri.www.util.S3Util;
import com.niri.www.util.AllowAnon;
import com.niri.www.entity.Project;
import com.niri.www.entity.ProjectHtml;
import com.niri.www.model.TokenValue;
import com.niri.www.service.ProjectService;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/prj")
public class ProjectController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final int PRJ_TYPE_DAM = 1;
    public static final int PRJ_STATUS_ACTIVE = 1;
    public static final int PRJ_STATUS_REMOVE = 0;

    private class FileResult
    {
        public boolean uploaded;
        public String fileUid;
        public String filePath;
    }

    @Autowired
    ServletContext context;

    @Autowired
    private ProjDefectService projDefectService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private S3Service s3Service;

    @AllowAnon
    @RequestMapping(value = "/dft", method = RequestMethod.GET)
    public JSONObject getDefects(String id) {
        logger.info("ProjectController.getDefects() projID = "+id);
        if(StringUtils.isEmpty(id))
            return null;
        return projDefectService.getProjDft(id);
    }

    @AllowAnon
    @RequestMapping(value = "/csv", method = RequestMethod.GET)
    public void exportCsv(HttpServletResponse response, @RequestParam(value = "id") String id) {
        try {
            logger.info("ProjectController.exportCsv() id = "+id);
            response.reset();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=defects.csv");
            response.setHeader("Access-Control-Allow-Origin", "*");

            try (Writer writer = new OutputStreamWriter(response.getOutputStream(), StandardCharsets.UTF_8)) {
                writer.write("id,longitude,latitude,length(cm),average-width(cm)\n");
                JSONObject jsonObject = projDefectService.getProjDft(id);
                if(jsonObject!=null){
                    JSONArray arr = jsonObject.getJSONArray("features");
                    //logger.info("features size = "+arr.size());
                    for (int i = 0; i< arr.size(); i++){
                        JSONObject jo = arr.getJSONObject(i);
                        String lon = jo.getJSONObject("geometry").getJSONArray("coordinates").getString(0);
                        String lat = jo.getJSONObject("geometry").getJSONArray("coordinates").getString(1);
                        String dftId = jo.getJSONObject("properties").getString("id");
                        String length = jo.getJSONObject("properties").getString("length");
                        String ave = jo.getJSONObject("properties").getString("ave-width");
                        DecimalFormat df = new DecimalFormat("#.#");
                        df.setRoundingMode(RoundingMode.CEILING);
                        double result = Double.valueOf(df.format(Double.parseDouble(ave)));
                        ave = String.valueOf(result);
                        if(!StringUtils.isEmpty(ave))
                            writer.write(String.format("%s,%s,%s,%s,%s\n",dftId, lon, lat, length, ave));
                        else
                            writer.write(String.format("%s,%s,%s,%s\n",dftId, lon, lat, length));
                    }
                }
                writer.flush();
            }
        } catch (IOException e) {
            logger.error("failed to export csv", e);
        } finally {
            //return ret;
        }
    }

    @RequestMapping(value = "/udft", method = RequestMethod.POST)
    public ResponseTemplate updateDefect(@RequestBody Defect dft, String id) {
        logger.info("ProjectController.updateDefect() projID = "+id);
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        if(StringUtils.isEmpty(id))
            return null;
        ret.setData(projDefectService.updateDft(id, dft));
        return ret;
    }

    @RequestMapping(value = "/pin", method = RequestMethod.GET)
    public JSONObject getProjPin(HttpServletResponse response, @RequestParam(value = "id") String id) {
        logger.info("ProjectController.getProjPin() id = "+id);
        if(StringUtils.isEmpty(id))
            return null;
        return projDefectService.getProjPin(id);
    }

    @RequestMapping(value = "/adpin", method = RequestMethod.POST)
    public ResponseTemplate addPin(@RequestBody JSONObject pin, @RequestParam(value = "id") String id) {
        logger.info("ProjectController.addPin() projID = "+id);
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(0);
        if(StringUtils.isEmpty(id) || pin == null)
            return ret;
        ret.setData(projDefectService.addPin(id, pin));
        return ret;
    }

    @RequestMapping(value = "/rmpin", method = RequestMethod.GET)
    public ResponseTemplate removePin(@RequestParam(value = "id") String id, @RequestParam(value = "pid") String pinID) {
        logger.info("ProjectController.removePin() projID = "+id);
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(0);
        if(StringUtils.isEmpty(id) || StringUtils.isEmpty(pinID))
            return ret;
        ret.setData(projDefectService.removePin(id, pinID));
        return ret;
    }

    @RequestMapping(value = "/adpq", method = RequestMethod.POST)
    public ResponseTemplate addPinQuest(@RequestBody JSONObject qust, @RequestParam(value = "id") String id, @RequestParam(value = "pid") String pinID) {
        logger.info("ProjectController.addPinQuest() projID = "+id);
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(0);
        if(StringUtils.isEmpty(id) || StringUtils.isEmpty(pinID) || qust == null)
            return ret;
        long result = projDefectService.addPinQuest(id, pinID, qust);
        if(result > 0){
            int read = qust.getIntValue("read");
            JSONObject prjObj = new JSONObject();
            prjObj.put("id", id);
            if(read == 0){
                prjObj.put("newQuest", 1);
            }else if(read == 1){
                prjObj.put("newAnswer", 1);
            }
            logger.info("ProjectController.addPinQuest() prjMap = "+JSONObject.toJSONString(prjObj));
            projectService.updateByMap(prjObj);
        }

        ret.setData(result);
        return ret;
    }

    @RequestMapping(value = "/rdpq", method = RequestMethod.GET)
    public ResponseTemplate readPinQuest(@RequestParam(value = "id") String id, @RequestParam(value = "pid") String pinID, @RequestParam(value = "read") int read) {
        logger.info("ProjectController.readPinQuest() projID = "+id);
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(0);
        if(StringUtils.isEmpty(id) || StringUtils.isEmpty(pinID))
            return ret;
        long result = projDefectService.readPinQuest(id, pinID, read);
        if(result > 0){
            JSONObject prjObj = new JSONObject();
            prjObj.put("id", id);
            if(read == 0){
                prjObj.put("newQuest", 0);
            }else if(read == 1){
                prjObj.put("newAnswer", 0);
            }
            //logger.info("ProjectController.readPinQuest() prjMap = "+JSONObject.toJSONString(prjObj));
            projectService.updateByMap(prjObj);
        }
        ret.setData(result);
        return ret;
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseTemplate getBy(@RequestBody Project prj, HttpServletRequest request) {
        logger.info("ProjectController.getBy() prj = "+JSONObject.toJSONString(prj));
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        TokenValue tokenValue = (TokenValue) request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);
        if(StringUtils.isEmpty(prj) || prj.getCountry() == null || (tokenValue.getType() & UserController.USER_TYPE_ADMIN) == 0)
            return null;
        if("all".equals(prj.getCountry()))
            prj.setCountry(null);
        if("all".equals(prj.getState()))
            prj.setState(null);

        prj.setStatus(PRJ_STATUS_ACTIVE);
        ret.setData(projectService.getBy(prj));
        ret.setMessage("get prj list succ");
        return ret;
    }

    @RequestMapping(value = "/usr_prj", method = RequestMethod.GET)
    public List<Project> getUserProjs(String user, HttpServletRequest request) {
        logger.info("ProjectController.getUserProjs() user = "+user);
        TokenValue tokenValue = (TokenValue) request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);
        if(StringUtils.isEmpty(user) || tokenValue.getUsername().equals(user) == false)
            return null;
        return projectService.getUserProjs(user);
    }

    @RequestMapping(value = "/uweb", method = RequestMethod.POST)
    public ResponseTemplate setPrjWebcontent(@RequestBody ProjectHtml prjHtml) {
        logger.info("ProjectController.setPrjWebcontent() id = "+prjHtml.getId());
        if(StringUtils.isEmpty(prjHtml.getId()))
            return null;
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage("update project html succ");
        List<ProjectHtml> list = projectService.getPrjHtml(prjHtml.getId());
        prjHtml.setStatus(PRJ_STATUS_ACTIVE);
        if(list.size() == 0) {
            projectService.addPrjHtml(prjHtml);
        }
        else {
            projectService.updatePrjHtml(prjHtml);
        }
        return ret;
    }

    @RequestMapping(value = "/web", method = RequestMethod.GET)
    public ResponseTemplate getPrjWebcontent(String id) {
        logger.info("ProjectController.getPrjWebcontent() id = "+id);
        if(StringUtils.isEmpty(id))
            return null;
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage(projectService.getPrjWebcontent(id));
        return ret;
    }

    @RequestMapping(value = "/asset", method = RequestMethod.GET)
    public ResponseTemplate getPrjAssets(String id) {
        logger.info("ProjectController.getPrjAssets() id = "+id);
        if(StringUtils.isEmpty(id))
            return null;
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage(projectService.getPrjAssets(id));
        return ret;
    }

    @RequestMapping(value = "/color", method = RequestMethod.GET)
    public ResponseTemplate getDftColor(String id) {
        logger.info("ProjectController.getDftColor() id = "+id);
        if(StringUtils.isEmpty(id))
            return null;
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage(projectService.getDftColor(id));
        return ret;
    }

    @RequestMapping(value = "/ech", method = RequestMethod.GET)
    public ResponseTemplate getECharts(String id) {
        logger.info("ProjectController.getECharts() id = "+id);
        if(StringUtils.isEmpty(id))
            return null;
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage(projectService.getECharts(id));
        return ret;
    }

    @AllowAnon
    @RequestMapping(value="/config")
    public void ueditorConfig(HttpServletRequest request, HttpServletResponse response) throws JSONException {

        logger.info("ProjectController.ueditorConfig()");

        String rootPath = "";
        response.setContentType("application/json");
        rootPath = request.getSession().getServletContext().getRealPath("/");
        try {
            String exec = new ActionEnter(request, rootPath).exec();
            PrintWriter writer = response.getWriter();
            writer.write(exec);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @AllowAnon
    @PostMapping(value = "/vueupload2")
    public Map<String, String> vueUpload2(@RequestParam(value = "file") MultipartFile file) {
        Map<String, String> map = new HashMap<>();
        String fileName = file.getOriginalFilename();

        String rootPath = context.getRealPath("resources/uploads");
        File dir = new File(rootPath);
        if (!dir.exists())
            dir.mkdirs();

        FileUtils.deleteQuietly(new File(rootPath+File.separator+fileName));
        File uploadedFile = new File(rootPath, fileName);

        logger.info("ProjectController.vueUpload2() fileName = "+fileName);

        try {
            file.transferTo(uploadedFile);
            String returnName = S3Util.getTickFilename(fileName);
            String s3Path = s3Service.upload2S3(rootPath+File.separator+fileName, returnName);
            map.put("url", s3Path);
            map.put("state", "SUCCESS");
            map.put("original", "");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            FileUtils.deleteQuietly(new File(rootPath+File.separator+fileName));
        }

        return map;
    }

    @AllowAnon
    @PostMapping(value = "/vueupload")
    public Map<String, String> vueUpload(@RequestParam(value = "file") MultipartFile file) {
        Map<String, String> map = new HashMap<>();
        String fileName = file.getOriginalFilename();

        String rootPath = context.getRealPath("resources/uploads");
        File dir = new File(rootPath);
        if (!dir.exists())
            dir.mkdirs();

        FileUtils.deleteQuietly(new File(rootPath+File.separator+fileName));
        File uploadedFile = new File(rootPath, fileName);

        logger.info("ProjectController.vueUpload() fileName = "+fileName);

        try {
            file.transferTo(uploadedFile);
            String returnName = S3Util.getDateFilename(fileName);
            String s3Path = s3Service.upload2Down(rootPath+File.separator+fileName, returnName);
            map.put("url", s3Path);
            map.put("state", "SUCCESS");
            map.put("original", "");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            FileUtils.deleteQuietly(new File(rootPath+File.separator+fileName));
        }

        return map;
    }

    @AllowAnon
    @PostMapping(value = {"/uploadimage", "/uploadvideo"})
    public Map<String, String> ueditorUploadImg(@RequestParam(value = "upfile") MultipartFile upfile) {
        Map<String, String> map = new HashMap<>();
        String fileName = upfile.getOriginalFilename();

        String rootPath = context.getRealPath("resources/uploads");
        File dir = new File(rootPath);
        if (!dir.exists())
            dir.mkdirs();

        FileUtils.deleteQuietly(new File(rootPath+File.separator+fileName));
        File uploadedFile = new File(rootPath, fileName);

        logger.info("ProjectController.uploadimage() fileName = "+fileName);

        try {
            upfile.transferTo(uploadedFile);
            String returnName = S3Util.getDateFilename(fileName);
            String s3Path = s3Service.upload2Down(rootPath+File.separator+fileName, returnName);
            map.put("url", s3Path);
            map.put("state", "SUCCESS");
            map.put("original", "");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            FileUtils.deleteQuietly(new File(rootPath+File.separator+fileName));
        }

        return map;
    }

    @AllowAnon
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public @ResponseBody String remove(@RequestParam String[] fileNames, String user) {
        String rootPath = context.getRealPath("resources/uploads");
        for(String fileName : fileNames) {
            String realName = user == null? fileName: user+"_"+fileName;
            FileUtils.deleteQuietly(new File(rootPath+File.separator+realName));
        }
        return "";
    }

    @AllowAnon
    @RequestMapping(value = "/chunkSave", method = RequestMethod.POST)
    public @ResponseBody String chunkSave(@RequestParam List<MultipartFile> files, String metadata, String user) throws JsonGenerationException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        long totalChunks = 0;
        long chunkIndex = 0;
        String uploadUid = "";
        String fileName = "";

        if(metadata == null){
            return "";
        }

        JsonNode rootNode = mapper.readTree(metadata);
        totalChunks = rootNode.path("totalChunks").getLongValue();
        chunkIndex = rootNode.path("chunkIndex").getLongValue();
        uploadUid = rootNode.path("uploadUid").getTextValue();
        fileName = rootNode.path("fileName").getTextValue();

        OutputStream output = null;

        String rootPath = context.getRealPath("resources/uploads");
        File dir = new File(rootPath);
        if (!dir.exists())
            dir.mkdirs();

        String realName = user == null? fileName: user+"_"+fileName;

        if(chunkIndex == 0){
            logger.info("ProjectController.chunkSave() remove old fileName = "+rootPath+File.separator+realName+" chunkIndex = "+chunkIndex+" totalChunks ="+totalChunks);
            FileUtils.deleteQuietly(new File(rootPath+File.separator+realName));
        }

        for (MultipartFile file : files) {
            byte[] bytes = file.getBytes();

            File serverFile = new File(rootPath, realName);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile,true));
            stream.write(bytes);
            stream.close();
            //logger.info("File: "+rootPath+File.separator+file.getName() + " finished...");
        }

        FileResult fileBlob = new FileResult();
        fileBlob.uploaded = totalChunks - 1 <= chunkIndex;
        fileBlob.fileUid = uploadUid;

        if(chunkIndex+1==totalChunks){
            logger.info("ProjectController.chunkSave() fileName = "+rootPath+File.separator+fileName+" chunkIndex = "+chunkIndex+" totalChunks ="+totalChunks);
            String s3Path = s3Service.upload2Down(rootPath+File.separator+fileName, S3Util.getDateFilename(fileName));
            fileBlob.filePath = s3Path;
            FileUtils.deleteQuietly(new File(rootPath+File.separator+realName));
        }

        return mapper.writeValueAsString(fileBlob);
    }

    @RequestMapping(value = "/up", method = RequestMethod.POST)
    public ResponseTemplate updatePrj(@RequestBody Project prj, HttpServletRequest request) {
        logger.info("ProjectController.updatePrj()");

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        TokenValue tokenValue = (TokenValue) request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);
        if(prj == null || (tokenValue.getType() & UserController.USER_TYPE_ADMIN) == 0)
            return ret;

        //prj.setUpdateDate(new Timestamp(System.currentTimeMillis()));
        ret.setData(projectService.updateByPrj(prj));
        ret.setMessage("update prj succ");
        return ret;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseTemplate addPrj(@RequestBody Project prj, HttpServletRequest request) {
        logger.info("ProjectController.addPrj()");

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(0);
        TokenValue tokenValue = (TokenValue) request.getAttribute(ConstantKit.REQUEST_TOKEN_VALUE);

        if(prj == null || StringUtils.isEmpty(prj.getId()) || (tokenValue.getType() & UserController.USER_TYPE_ADMIN) == 0)
            return ret;

        Project target = new Project();
        target.setId(prj.getId());
        List<Project> list = projectService.getBy(target);
        if(list!=null && list.size()>0){
            //ret.setData(1);
            return ret;
        }
        target.setId(null);
        target.setName(prj.getName());
        list = projectService.getBy(target);
        if(list!=null && list.size()>0){
            //ret.setData(1);
            return ret;
        }

        if(prj.getType()==0) prj.setType(PRJ_TYPE_DAM);
        prj.setStatus(PRJ_STATUS_ACTIVE);

        ret.setData(projectService.addPrj(prj));
        ret.setMessage("add user succ");
        return ret;
    }
}
