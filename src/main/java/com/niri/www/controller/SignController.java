package com.niri.www.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.niri.www.model.ResponseTemplate;
import com.niri.www.util.ConstantKit;
import com.niri.www.util.JedisPoolCache;
import com.niri.www.util.AllowAnon;
import com.niri.www.entity.User;
import com.niri.www.model.TokenValue;
import com.niri.www.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.UUID;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/sign")
public class SignController {

    Logger logger = LoggerFactory.getLogger(SignController.class);

    @Resource
    UserService userService;

    @Autowired
    private JedisPoolCache jedisPoolCache;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    @AllowAnon
    public ResponseTemplate login(HttpServletRequest request, @RequestParam(value = "username") String username, @RequestParam(value = "password") String password) {

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage(HttpStatus.OK.getReasonPhrase());
        JSONObject result = new JSONObject();

        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            result.put("status", "login failed: invalid username or password");
            ret.setData(result);
            return ret;
        }

        password = DigestUtils.sha256Hex(password);
        User user = userService.getUser( username, password );

        logger.info("user login: "+ username);

        if (user != null) {

            String token = UUID.randomUUID().toString().replace("-", "");

            TokenValue tv = new TokenValue(username, user.getType(), System.currentTimeMillis());

            Jedis jedis = jedisPoolCache.getCache();
            jedis.set(token, JSON.toJSONString(tv));
            jedis.expire(token, ConstantKit.TOKEN_EXPIRE_TIME);
            jedis.close();

            result.put("status", "login succ");
            result.put("token", token);
            result.put("type", user.getType());
            result.put("email", user.getEmail());
            result.put("phone", user.getPhone());
            result.put("country", user.getCountry());
            result.put("province", user.getProvince());

            Date currTime = new Date();
            logger.info(user.getUsername()+" login time ="+currTime.toGMTString() + " ip = "+request.getRemoteAddr());
        } else {
            result.put("status", "login failed: invalid username or password");
        }

        ret.setData(result);
        return ret;
    }

}
