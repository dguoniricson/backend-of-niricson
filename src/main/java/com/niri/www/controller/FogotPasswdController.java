package com.niri.www.controller;

import com.alibaba.fastjson.JSON;
import com.niri.www.dao.FgtPwdMapper;
import com.niri.www.dao.UserMapper;
import com.niri.www.entity.FgtPwd;
import com.niri.www.entity.User;
import com.niri.www.model.ResponseTemplate;
import com.niri.www.util.Md5TokenGenerator;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/fgt")
public class FogotPasswdController {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Resource
    private UserMapper userMapper;

    @Resource
    private FgtPwdMapper fgtPwdMapper;

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ResponseTemplate resetPasswd(@RequestParam String id, String passwd, HttpServletRequest request) {

        logger.info("FogotPasswdController.resetPasswd() id = "+id+ " passwd = "+passwd);

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage("forgot reset succ");

        FgtPwd fpCond = new FgtPwd();
        fpCond.setToken(id);
        FgtPwd fpTarget = fgtPwdMapper.getBy(fpCond);

        if(fpTarget == null || fpTarget.getStatus() == 0){
            ret.setMessage("invalid link");
            return ret;
        }

        User userCond = new User();
        userCond.setEmail(fpTarget.getEmail());
        if(!StringUtils.isEmpty(passwd)){
            passwd = DigestUtils.sha256Hex(passwd);
        }
        userCond.setPassword(passwd);
        userMapper.updateByEmail(userCond);

        fpTarget.setStatus(0);
        fgtPwdMapper.update(fpTarget);

        return ret;
    }

    @RequestMapping(value = "/req", method = RequestMethod.GET)
    public ResponseTemplate reqPasswd(@RequestParam String email, HttpServletRequest request) {

        logger.info("FogotPasswdController.reqPasswd()");

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage("forgot request succ");

        if(StringUtils.isEmpty(email))
            return ret;

        User user = new User();
        user.setEmail(email);

        List<User> list = userMapper.getBy(user);
        if(list == null){
            logger.info("no match for this email: "+email);
            return ret;
        }

        User targetUser = list.get(0);
        final String uuid = UUID.randomUUID().toString().replace("-", "");
        String token = Md5TokenGenerator.generate(targetUser.getUsername()) + uuid;

        FgtPwd fp = new FgtPwd();
        fp.setEmail(email);
        fp.setIp(request.getRemoteAddr());
        fp.setStatus(1);
        fp.setToken(token);

        FgtPwd target = fgtPwdMapper.getFgtPwd(email);
        if(target != null){
            logger.info("update tb_forgot_passwd");
            fgtPwdMapper.update(fp);
        } else {
            logger.info("insert tb_forgot_passwd: "+JSON.toJSONString(fp) );
            fgtPwdMapper.insert(fp);
        }

        return ret;
    }
}
