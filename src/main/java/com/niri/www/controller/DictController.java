package com.niri.www.controller;


import com.niri.www.model.ResponseTemplate;
import com.niri.www.service.DictService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/dic")
public class DictController {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private DictService dictService;

    @RequestMapping(value = "/kv", method = RequestMethod.GET)
    public ResponseTemplate getKV(String key) {
        logger.info("DictController.getKV() key = "+key);
        if(StringUtils.isEmpty(key))
            return null;
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setMessage(dictService.getKV(key));
        return ret;
    }

    @RequestMapping(value = "/cty", method = RequestMethod.GET)
    public ResponseTemplate getCountry() {
        logger.info("DictController.getCountry()");

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(dictService.getCountry());
        return ret;
    }

    @RequestMapping(value = "/prv", method = RequestMethod.GET)
    public ResponseTemplate getProvince(String code) {
        logger.info("DictController.getProvince()");

        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());
        ret.setData(dictService.getProvince(code));
        return ret;
    }
}
