package com.niri.www.controller;

import java.io.*;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.*;
import com.niri.www.model.AWSNode;
import com.niri.www.model.ResponseTemplate;
import com.niri.www.util.AllowAnon;
import com.niri.www.model.S3NotifyReq;
import com.niri.www.service.AWSS3Service;
import com.niri.www.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value= "/api/s3")
public class AWSS3Ctrl {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private AWSS3Service service;

    private final AmazonS3 s3 = AmazonS3ClientBuilder.standard()
            .withRegion(Regions.US_EAST_2)
            .withCredentials(new ProfileCredentialsProvider())
            .build();

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Autowired
    MailService mailService;

    @RequestMapping(value = "/notify", method = RequestMethod.POST)
    public ResponseTemplate notifyAdmin(@RequestBody S3NotifyReq req){
        String[] toPerson = new String[]{
        "wjing@niricson.com"
          /*
          ,"jfowler@niricson.com"
          ,"kang@niricson.com"
          //*/
        };
        ResponseTemplate ret = new ResponseTemplate();
        ret.setCode(HttpStatus.OK.value());

        logger.info("notifyAdmin() req = "+JSONObject.toJSONString(req));
        String[] pathList = req.getPath().split("/");
        String folder = pathList.length>1? pathList[pathList.length-1]:"";
        String content = "Dear Admin,\n\nDrone operator "+req.getAcc()+" has finished uploading to \n\nbucket: "+req.getBucket()+ "\nproject folder: "+ folder +"\n\nThanks,";
        String msg = mailService.sendSimpleMail(toPerson, "Message from Niricson Cloud", content);
        ret.setMessage(msg);
        return ret;
    }

    //@AllowAnon
    @PostMapping(value= "/upload")
    public Map<String, Object> uploadFile(@RequestPart(value= "file") final MultipartFile multipartFile, @RequestParam(value="keyName") String keyName) {
        service.uploadFile(multipartFile,keyName);
        Map<String, Object> map = new HashMap<>();
        map.put("url", "");
        map.put("status", HttpStatus.OK);
        return map;
    }

    @GetMapping(value= "/renameObject")
    public Map<String, Object> renameObject(@RequestParam(value="sourceKey") String sourceKey,
                                            @RequestParam(value="destinationKey") String destinationKey) {
        Map<String, Object> map = new HashMap<>();
        try {
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, sourceKey, bucketName, destinationKey);
            s3.copyObject(copyObjRequest);
            s3.deleteObject(bucketName, sourceKey);
        } catch (AmazonServiceException e) {
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("msg", "aws error");
            e.printStackTrace();
            return map;
        } catch (SdkClientException e) {
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("msg", "sdk error");
            e.printStackTrace();
            return map;
        }
        map.put("status", HttpStatus.OK);
        map.put("msg", "rename success");
        return map;
    }

    @GetMapping(value= "/createFolder")
    public Map<String, Object> createFolder(@RequestParam(value="folderName") String folderName) {
        createFolder(bucketName,folderName, s3);
        Map<String, Object> map = new HashMap<>();
        map.put("status", HttpStatus.OK);
        return map;
    }

    public static void createFolder(String bucketName, String folderName, AmazonS3 s3) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);

        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                folderName + "/", emptyContent, metadata);
        s3.putObject(putObjectRequest);
    }

    @GetMapping(value= "/delete")
    public Map<String, Object> deleteFile(@RequestParam(value="keyName") String keyName) {
        Map<String, Object> map = new HashMap<>();
        logger.info("AWSS3Ctrl.deleteFile() key = "+keyName);
        try {
            s3.deleteObject(bucketName, keyName);
            map.put("status", HttpStatus.OK);
            map.put("msg", "file deleted");
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("msg", "file deletion failed");
        }
        return map;
    }

    @GetMapping(value= "/batchDelete")
    public Map<String, Object> batchDelete(@RequestParam(value="nodeKeyNames") String[] keyNames) {
        Map<String, Object> map = new HashMap<>();
        try {
            DeleteObjectsRequest dor = new DeleteObjectsRequest(bucketName)
                    .withKeys(keyNames);
            s3.deleteObjects(dor);
            map.put("status", HttpStatus.OK);
            map.put("msg", "file deleted successfully");
        } catch (AmazonServiceException e) {
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("msg", "file deletion failed");
        }
        return map;
    }

    @AllowAnon
    @GetMapping(value= "/download2")
    public String downFromS3(HttpServletResponse response, @RequestParam(value= "keyName") final String key
    ) {
        Map<String, Object> map = new HashMap<>();
        logger.info("downFromS3() keyname = "+key);
        S3Object fullObject = null;
        try {
          String bktName = "niri-test";
          fullObject = s3.getObject(new GetObjectRequest(bktName, key));
          Long fileSize = s3.getObjectMetadata(bktName, key).getContentLength();
          InputStream objectData = fullObject.getObjectContent();

          response.reset();
          response.setCharacterEncoding("UTF-8");
          response.setContentType("application/octet-stream");
          response.setHeader("Content-Disposition", "attachment;filename=" + key);
          response.setHeader("Access-Control-Allow-Origin", "*");
          response.setContentLength(fileSize.intValue());

          BufferedInputStream bis = null;
          OutputStream out = null;
          try {
            bis = new BufferedInputStream(objectData);
            out = response.getOutputStream();
            byte[] buff = new byte[4096];
            int index = 0;
            int count = 0;
            while((index= bis.read(buff))!= -1){
              count += index;
              out.write(buff, 0, index);
              out.flush();
            }
            logger.info("total file size = "+count);
            return "download succ";
          } catch (Exception e) {
            e.printStackTrace();
          } finally {
            if (bis != null) {
              try {
                bis.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
            if (out != null) {
              try {
                out.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
            if (objectData != null) {
              try {
                objectData.close();
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
          }
        } catch (AmazonServiceException e) {
          e.printStackTrace();
        } catch (SdkClientException e) {
          e.printStackTrace();
        }
        return "download failed";
    }

    @AllowAnon
    @GetMapping(value= "/download")
    public String downloadFileOld(HttpServletResponse response, @RequestParam(value= "keyName") final String key
    ) {
        Map<String, Object> map = new HashMap<>();
        logger.info("downloadFile() keyname = "+key);
        S3Object fullObject = null;
        try {

            fullObject = s3.getObject(new GetObjectRequest(bucketName, key));
            InputStream objectData = fullObject.getObjectContent();
            Long fileSize = s3.getObjectMetadata(bucketName, key).getContentLength();

            response.reset();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + key);
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setContentLength(fileSize.intValue());

            BufferedInputStream bis = null;
            OutputStream out = null;
            try {
                bis = new BufferedInputStream(objectData);
                out = response.getOutputStream();
                byte[] buff = new byte[4096];
                int index = 0;
                int count = 0;
                while((index= bis.read(buff))!= -1){
                    count += index;
                    out.write(buff, 0, index);
                    out.flush();
                }
                logger.info("total file size = "+count);
                return "download succ";
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (objectData != null) {
                    try {
                        objectData.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (AmazonServiceException e) {
            e.printStackTrace();
        } catch (SdkClientException e) {
            e.printStackTrace();
        }
        return "download failed";
    }

    @GetMapping(value= "/list")
    public Map<String, Object> listFile(@RequestParam("userName") String userName){
        Map<String, Object> map = new HashMap<>();

        logger.info("listFile() userName = "+userName);

        ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(userName+"/");

        ListObjectsV2Result result = s3.listObjectsV2(req);
        List<S3ObjectSummary> objects = result.getObjectSummaries();
        if(objects.size()==0) {
            createFolder(bucketName,userName, s3);
        }

        List<AWSNode> treeList = new ArrayList<AWSNode>();
        map.put("bucketName", bucketName);
        map.put("JsonTree", buildZtree(objects,treeList));
        return map;
    }

    private List<AWSNode> buildZtree(List<S3ObjectSummary> objects, List<AWSNode> treeList) {
        int pId = 1;
        int id = 1;
        logger.info("buildZtree()");
        Map<String, Integer> parentMap = new HashMap<>();

        Set<String> folderSet = new TreeSet<>();
        List<String> fileList = new ArrayList<>();
        for (int i=0; i< objects.size(); i++) {
            S3ObjectSummary o = objects.get(i);

            if(!o.getKey().endsWith("/")){
                fileList.add(o.getKey());
                String[] pathList = o.getKey().split("/");
                if(pathList.length>1){
                    String basePath = pathList[0] + "/";
                    folderSet.add(basePath);
                    for(int j=1; j<pathList.length-1; j++){
                      basePath = basePath + pathList[j] + "/";
                      folderSet.add(basePath);
                    }
                }
            } else {
                folderSet.add(o.getKey());
            }
        }// loop

        int count = 0;
        for (String o : folderSet) {
                AWSNode node = new AWSNode();
                pId = id;
                node.setId(pId);
                String[] names = o.split("/");
                parentMap.put(o.substring(0, o.lastIndexOf("/")), id);
                if(names.length>1)
                {
                    String parentName = o.substring(0, o.lastIndexOf('/',o.lastIndexOf("/")-1));
                    node.setpId(parentMap.get(parentName));
                }else
                {
                    node.setpId(pId);
                }

                node.setName(names[names.length-1]);
                node.setIsParent(true);
                node.setKeyName(o);
                treeList.add(node);
                id++;
        }
        for(String o: fileList){
            id++;
            AWSNode node = new AWSNode();
            node.setId(id);
            String[] names = o.split("/");
            if(names.length>1)
            {
              node.setpId(parentMap.get(o.substring(0, o.lastIndexOf("/"))));
            }else if (names.length==1) {
              node.setpId(1);
            }else
            {
              node.setpId(pId);
            }
            node.setName(names[names.length-1]);
            node.setIsParent(false);
            node.setKeyName(o);
            treeList.add(node);
        }

        return treeList;
    }
}
