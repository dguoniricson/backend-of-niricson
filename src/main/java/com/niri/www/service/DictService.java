package com.niri.www.service;

import com.niri.www.dao.DictMapper;
import com.niri.www.entity.Country;
import com.niri.www.entity.Province;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("dictService")
public class DictService {
    @Resource
    private DictMapper dictMapper;

    public String getKV(String key){
        return dictMapper.getKV(key);
    }
    public List<Country> getCountry(){ return dictMapper.getCountry(); }
    public List<Province> getProvince(String ctyCode){ return dictMapper.getProvince(ctyCode); }
}
