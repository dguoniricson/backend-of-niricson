package com.niri.www.service;

import com.niri.www.util.ResourceReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("mailService")
public class MailService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender mailSender;

    @Value("${mail.fromMail.addr}")
    private String from;

    public String sendSimpleMail(String[] toPerson, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(toPerson);
        message.setSubject(subject);
        message.setText(content);

        try {
            mailSender.send(message);
            logger.info("simple email sent ...");
        } catch (Exception ex) {
            logger.error("sending email ex: ", ex);
            return "send email failed";
        }
        return "send email succ";
    }

    public String sendEmailWithAttachment(String toPerson, String subject) //throws MessagingException, IOException
    {
        MimeMessage msg = mailSender.createMimeMessage();
        try
        {
            // true = multipart message
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);

            helper.setTo(toPerson);

            helper.setSubject(subject);

            // default = text/plain
            //helper.setText("Check attachment for image!");

            // true = text/html
            helper.setText("<h3>Please click the link to reset your password.</h3>", true);
            String content = ResourceReader.getContent("/templates/reset-passwd-email.html");
            if (StringUtils.isEmpty(content) == false)
                helper.setText(content, true);
            //helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

            mailSender.send(msg);
        }catch(Exception ex){
                ex.printStackTrace();
        }
        return "send email succ";
    }
}
