package com.niri.www.service;

import com.alibaba.fastjson.JSONObject;
import com.niri.www.dao.ProjectMapper;
import com.niri.www.entity.Project;
import com.niri.www.entity.ProjectHtml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("projectService")
public class ProjectService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private ProjectMapper projectMapper;

    public List<Project> getUserProjs(String user){
        List<Project> list = projectMapper.getUserProjs(user);
        logger.info("list: "+ list.size());
        return list;
    }

    public List<Project> getBy(Project prj){
        List<Project> list = projectMapper.getBy(prj);
        logger.info("list: "+ list.size());
        return list;
    }

    public String getPrjWebcontent(String id){
        return projectMapper.getPrjWebcontent(id);
    }

    public List<ProjectHtml> getPrjHtml(String id){
        return projectMapper.getPrjHtml(id);
    }

    public int updatePrjHtml(ProjectHtml projectHtml){
        return projectMapper.updatePrjHtml(projectHtml);
    }

    public int addPrjHtml(ProjectHtml projectHtml) { return projectMapper.addPrjHtml(projectHtml);}

    @Transactional
    public int updateByMap(JSONObject map){
        return projectMapper.updateByMap(map);
    }

    @Transactional
    public int updateByPrj(Project prj){
        return projectMapper.updateByPrj(prj);
    }

    @Transactional
    public int addPrj(Project prj) { return projectMapper.addPrj(prj);}

    public String getPrjAssets(String id){
        return projectMapper.getPrjAssets(id);
    }

    public String getDftColor(String id) {
      return projectMapper.getDftColor(id);
    }

    public String getECharts(String id) {
    return projectMapper.getECharts(id);
  }


}
