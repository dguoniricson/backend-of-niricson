package com.niri.www.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;

@Service("awsS3Service")
public class AWSS3Service {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
            .withRegion(Regions.US_EAST_2)
                    .withCredentials(new ProfileCredentialsProvider())
            .build();

    @Value("${aws.s3.bucket}")
    private String bucketName;
    @Autowired
    private Environment env;

    @Async
    public void uploadFile(final MultipartFile multipartFile, String keyName) {

        try {
            final File file = convertMultiPartFileToFile(multipartFile);
            uploadFileToS3Bucket(bucketName, file, keyName);
            //logger.info("File upload is completed.");
            file.delete();	// To remove the file locally created in the project folder.
        } catch (final AmazonServiceException ex) {
            logger.info("File upload is failed.");
            logger.error("Error= {} while uploading file.", ex.getMessage());
        }
    }

    private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
        final File file = new File(multipartFile.getOriginalFilename());
        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(multipartFile.getBytes());
        } catch (final IOException ex) {
            logger.error("Error converting the multi-part file to file= ", ex.getMessage());
        }
        return file;
    }

    private void uploadFileToS3Bucket(final String bucketName, final File file, String keyName) {

        //logger.info("uploadFileToS3Bucket() bucket = "+bucketName + " keyName = "+keyName + " file = "+file.getName());
        final String fileName = keyName + "/" + file.getName();
        //logger.info("Uploading file with name= " + fileName);
        final PutObjectRequest putRequest = new PutObjectRequest(bucketName, fileName, new File(file.getName()));
        amazonS3.putObject(putRequest);
    }

    public void createFolder(String bucketName, String folderName, AmazonS3 s3) {
        // create meta-data for your folder and set content-length to 0
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        // create empty content
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        // create a PutObjectRequest passing the folder name suffixed by /
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                folderName + "/", emptyContent, metadata);
        // send request to S3 to create folder
        s3.putObject(putObjectRequest);
    }

    @Async
    public byte[] downloadFile(final String keyName) {
        byte[] content = null;
        logger.info("Downloading an object with key= " + keyName);
        final S3Object s3Object = amazonS3.getObject(bucketName, keyName);
        final S3ObjectInputStream stream = s3Object.getObjectContent();
        try {
            content = IOUtils.toByteArray(stream);
            //logger.info("File downloaded successfully.");
            s3Object.close();
        } catch(final IOException ex) {
            logger.info("IO Error Message= " + ex.getMessage());
        }
        return content;
    }
}
