package com.niri.www.service;

import com.alibaba.fastjson.JSONObject;
import com.niri.www.dao.UserMapper;
import com.niri.www.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserService {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Resource
    private UserMapper userMapper;

    public User getUser(String un, String pwd){
        return userMapper.getUser(un, pwd);
    }

    public JSONObject getUserConfig(String un){
        JSONObject ret = new JSONObject();
        User user = userMapper.getUserConfig(un);
        if(user != null){
            ret.put("email", user.getEmail());
            ret.put("phone", user.getPhone());
        }
        return ret;
    }

    public int updateByUser(User usr){
        return userMapper.updateByUser(usr);
    }

    public List<User> getBy(User user){
        return userMapper.getBy(user);
    }

    public List<User> getByOr(User user){
        return userMapper.getByOr(user);
    }

    public int addUser(User user) { return userMapper.addUser(user);}
}
