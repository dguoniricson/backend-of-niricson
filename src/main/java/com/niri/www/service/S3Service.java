package com.niri.www.service;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.URL;

@Service("s3Service")
public class S3Service {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public String upload2S3(String from, String fileName){
        String finalPath = null;
        Regions clientRegion = Regions.US_EAST_2;
        String bucketName = "niri-test";
        String keyName = "file/"+fileName;
        String filePath = from;
        logger.info("S3Service.upload2S3() begin: "+from);
        try {
          AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
            .withRegion(clientRegion)
            .withCredentials(new ProfileCredentialsProvider())
            .build();

          s3Client.putObject(new PutObjectRequest(bucketName, keyName, new File(filePath)).withCannedAcl(CannedAccessControlList.Private));
          URL picUrl = s3Client.getUrl(bucketName, keyName);

          finalPath = keyName;
          logger.info("S3Service.upload2S3() complete: "+finalPath);
        } catch (Exception ex){
          ex.printStackTrace();
        } finally {
          return finalPath;
        }
    }

    public String upload2Down(String from, String fileName){
        String finalPath = null;
        Regions clientRegion = Regions.US_EAST_2;
        String bucketName = "niri-test";
        String keyName = "down/"+fileName;
        String filePath = from;
        logger.info("S3Service.upload2Down() begin: "+from);
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .withCredentials(new ProfileCredentialsProvider())
                    .build();

            s3Client.putObject(new PutObjectRequest(bucketName, keyName, new File(filePath)).withCannedAcl(CannedAccessControlList.PublicRead));
            URL picUrl = s3Client.getUrl(bucketName, keyName);

            finalPath = picUrl.toExternalForm();
            logger.info("S3Service.upload2Down() complete: "+finalPath);
        } catch (Exception ex){
            ex.printStackTrace();
        } finally {
            return finalPath;
        }
    }
}
