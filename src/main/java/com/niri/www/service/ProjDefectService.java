package com.niri.www.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import com.niri.www.model.Defect;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

@Service("projDefectService")
public class ProjDefectService {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    static final String pin_collect = "proj_pin";
    static final String dft_collect = "proj_defects";


    @Autowired
    private MongoTemplate mongoTemplate;

    public JSONObject getProjDft(String projID) {
        Query query = new Query();
        query.addCriteria(Criteria.where("proj_id").is(projID));
        JSONObject pd = mongoTemplate.findOne(query, JSONObject.class, dft_collect);
        return pd;
    }

    public long updateDft(String projID, Defect dft){
        long ret = 0;
        if(dft == null || StringUtils.isEmpty(dft.getId()))
            return ret;
        String dftID = dft.getId();
        Query query = query(where("features.properties.id").is(dftID).and("proj_id").is(projID));
        if(!StringUtils.isEmpty(dft.getAw())){
            Update update = update("features.$.properties.ave-width", Double.valueOf(dft.getAw()));
            UpdateResult uret = mongoTemplate.updateFirst(query, update, dft_collect);
            ret = uret.getModifiedCount();
        }
        if(!StringUtils.isEmpty(dft.getLeng())){
            Update update = update("features.$.properties.length", Double.valueOf(dft.getLeng()));
            UpdateResult uret = mongoTemplate.updateFirst(query, update, dft_collect);
            ret = uret.getModifiedCount();
        }
        return ret;
    }

    public JSONObject getProjPin(String projID) {
        Query query = new Query();
        query.addCriteria(Criteria.where("proj_id").is(projID));
        JSONObject pd = mongoTemplate.findOne(query, JSONObject.class, pin_collect);
        return pd;
    }

    public long addPin(String projID, JSONObject pin){
        long ret = 0;
        Query query = new Query();
        query.addCriteria(Criteria.where("proj_id").is(projID));
        JSONObject pd = mongoTemplate.findOne(query, JSONObject.class, pin_collect);
        if(pd == null){
            JSONObject pinObj = new JSONObject();
            pinObj.put("proj_id", projID);
            JSONArray pinList = new JSONArray();
            pinList.add(pin);
            pinObj.put("pin", pinList);
            mongoTemplate.insert(pinObj, pin_collect);
            ret = 1;
            return ret;
        }

        String pid = pin.getString("id");
        if(StringUtils.isEmpty(pid))
            return ret;
        logger.info("pin id = "+pid);
        query = Query.query(Criteria.where("proj_id").is(projID).and("pin.id").is(pid));

        UpdateResult uret = mongoTemplate.getCollection(pin_collect).updateOne( Filters.eq("proj_id", projID), Updates.addToSet("pin", pin));
        logger.info("match = "+uret.getMatchedCount() + " modify = "+uret.getModifiedCount());
        ret = uret.getModifiedCount();
        return ret;

    }

    public long removePin(String projID, String pinID){
        long ret = 0;

        logger.info("ProjDefectService.removePin() projID = "+projID + " pinId = "+pinID);

        Bson query = new Document().append("proj_id", projID);
        Bson fields = new Document().append("pin", new Document().append( "id", pinID));
        Bson update = new Document("$pull", fields);

        UpdateResult uret = mongoTemplate.getCollection(pin_collect).updateOne( query, update );
        
        logger.info("match = "+uret.getMatchedCount() + " modify = "+uret.getModifiedCount());
        ret = uret.getModifiedCount();
        return ret;
    }

    public long readPinQuest(String projID, String pinID, int read){
        long ret = 0;

        // if read == 0, set to 2 (admin read all)
        // if read == 1, set to 2 (cust  read all)
        Bson filters = Filters.and(Filters.eq("proj_id", projID),
                Filters.elemMatch(
                        "pin",
                        Filters.and(
                                Filters.eq("id",pinID),
                                Filters.elemMatch("qa",Filters.eq("read",read))
                        )
                )
        );

        Bson update = Updates.set("pin.$.qa.$[qa].read", 2.0);
        Bson filter = Filters.eq("qa.read", read);

        UpdateOptions updateOptions = new UpdateOptions().arrayFilters(Collections.singletonList(filter));

        UpdateResult uret = mongoTemplate.getCollection(pin_collect).updateMany(filters,update,updateOptions);
        logger.info("match = "+uret.getMatchedCount() + " modify = "+uret.getModifiedCount());
        ret = uret.getModifiedCount();

        return ret;
    }

    public long addPinQuest(String projID, String pinID, JSONObject pinQ){
        long ret = 0;

        Query query = new Query();
        query = Query.query(Criteria.where("proj_id").is(projID).and("pin.id").is(pinID));

        JSONObject qaObj = new JSONObject();
        qaObj.put("dt", pinQ.getString("dt"));
        qaObj.put("from", pinQ.getString("from"));
        qaObj.put("ct", pinQ.getString("ct"));
        qaObj.put("read", pinQ.getDouble("read"));

        Bson filters = Filters.and(
                Filters.eq("proj_id", projID),
                Filters.elemMatch("pin", Filters.eq("id", pinID)
                )
        );
        Bson update = Updates.addToSet("pin.$.qa", qaObj);
        UpdateResult uret = mongoTemplate.getCollection(pin_collect).updateOne(filters, update, new UpdateOptions().upsert(true));

        logger.info("match = "+uret.getMatchedCount() + " modify = "+uret.getModifiedCount());
        ret = uret.getModifiedCount();
        return ret;
    }


}
